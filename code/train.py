from pathlib import Path
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
import pandas as pd
import tensorflow as tf
from tensorflow import keras


if __name__ == '__main__':
    # Constants
    features = ['ft[0]', 'ft[1]']
    target = 'Potability'
    dpath = Path(__file__).parents[1].joinpath('data')
    # Read data
    df = pd.read_csv(str(dpath.joinpath('train_big_ppc.csv')))
    df_test = pd.read_csv(str(dpath.joinpath('test_ppc.csv')))
    # Define model
    model = keras.Sequential([keras.layers.InputLayer(input_shape=(2,)),
                              keras.layers.Dense(32),
                              keras.layers.Dense(1)])
    # Train
    model.compile(optimizer='adam',
                  loss=tf.keras.losses.BinaryCrossentropy(from_logits=True),
                  metrics=['accuracy'])
    model.fit(df[features], df[target], epochs=3, batch_size=128)
    # Evaluate
    loss, accuracy = model.evaluate(df_test[features], df_test[target])
    print(f"Evaluation : loss={loss:.6f} accuracy={accuracy:.2%}")
    # Save model
    model.save(str(dpath.parent.joinpath("models", "my_keras_model")))

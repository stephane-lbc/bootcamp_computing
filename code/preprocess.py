from pathlib import Path
import os
from pyspark.sql import SparkSession
import pyspark.sql.functions as F
from pyspark.sql.types import FloatType, IntegerType
from pyspark.ml.feature import MinMaxScaler, PCA, VectorAssembler
from pyspark.ml.functions import vector_to_array


if __name__ == '__main__':
    # Constants
    features = ['Hardness', 'Solids', 'Chloramines', 'Conductivity']
    target = 'Potability'
    dpath = Path(__file__).parents[1].joinpath('data')
    # Create SparkSession
    spark = SparkSession.builder.appName("BootcampPreprocessingApp").getOrCreate()
    # Read data
    df = spark.read.option('header', True).csv(str(dpath.joinpath('train_big.csv')))
    df_test = spark.read.option('header', True).csv(str(dpath.joinpath('test.csv')))
    # Cast columns
    for ft in features:
        df = df.withColumn(ft, F.col(ft).cast(FloatType()))
        df_test = df_test.withColumn(ft, F.col(ft).cast(FloatType()))
    df = df.withColumn(target, F.col(target).cast(IntegerType()))
    df_test = df_test.withColumn(target, F.col(target).cast(IntegerType()))
    # Prepare for preprocessing
    assembler = VectorAssembler(inputCols=features, outputCol="features")
    df = assembler.transform(df)
    df_test = assembler.transform(df_test)
    # PCA
    red = PCA(k=2, inputCol='features', outputCol='pca_features')
    red = red.fit(df)
    df = red.transform(df)
    df_test = red.transform(df_test)
    # Scale
    scaler = MinMaxScaler(inputCol='pca_features', outputCol='scaled_features')
    scaler = scaler.fit(df)
    df = scaler.transform(df)
    df_test = scaler.transform(df_test)
    # Reformat dataframe
    df = df.withColumn('ft', vector_to_array('scaled_features'))\
           .select([F.col('ft')[i] for i in range(2)] + [target])
    df_test = df_test.withColumn('ft', vector_to_array('scaled_features'))\
                     .select([F.col('ft')[i] for i in range(2)] + [target])
    # Save preprocessed data
    #df.write.csv('', header=True)
    df.toPandas().to_csv(str(dpath.joinpath('train_big_ppc.csv')), index=False)
    df_test.toPandas().to_csv(str(dpath.joinpath('test_ppc.csv')), index=False)
    # Stop spark application
    spark.stop()



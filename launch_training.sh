#!/bin/bash


python code/train.py

DPATH=$(dirname "$0")          # relative
DPATH=$(cd "$DPATH" && pwd)    # absolutized and normalized
MODEL_PATH=$DPATH/models/my_keras_model

mkdir -p ${MODEL_PATH}/1
mv ${MODEL_PATH}/assets ${MODEL_PATH}/1/
mv ${MODEL_PATH}/keras_metadata.pb ${MODEL_PATH}/1/
mv ${MODEL_PATH}/saved_model.pb ${MODEL_PATH}/1/
mv ${MODEL_PATH}/variables ${MODEL_PATH}/1/


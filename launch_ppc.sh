#!/bin/bash

spark-submit \
	--deploy-mode=client \
	--num-executors=4 \
	code/preprocess.py

#!/bin/bash


docker pull tensorflow/serving

DPATH=$(dirname "$0")          # relative
DPATH=$(cd "$DPATH" && pwd)    # absolutized and normalized
MNAME=my_keras_model
echo

docker run -p 8501:8501 \
	-v "${DPATH}/models/${MNAME}:/models/${MNAME}" \
	-e MODEL_NAME=${MNAME} \
	-t tensorflow/serving
